create table if not exists Taco_users (
	id identity,
	username varchar(50) not null,
	password varchar(255) not null,
	enabled boolean not null,
	fullname varchar(50),
	street varchar(50),
	city varchar(50),
	state varchar(50),
	zip varchar(50),
	phone_number varchar(50)
);
create index if not exists username_index on Taco_users(username);

create table if not exists Authority (
	id identity,
	authority varchar(255) not null
);

create table if not exists Taco_users_authorities (
	taco_users_id bigint not null,
	authorities_id varchar(50) not null
);
alter table Taco_users_authorities add constraint if not exists taco_users_id_fk foreign key(taco_users_id) references Taco_users(id) ;
alter table Taco_users_authorities add constraint if not exists authorities_id_fk foreign key(authorities_id) references Authority(id) ;

----------------

create table if not exists Ingredient (
	id varchar(4) primary key not null,
	name varchar(25) not null,
	type varchar(10) not null
);

create table if not exists Taco (
	id identity,
	name varchar(50) not null,
	created_At timestamp not null
);

create table if not exists Taco_Ingredients (
	taco_id bigint not null,
	ingredients_id varchar(4) not null
);

alter table Taco_Ingredients add foreign key (taco_id) references Taco(id);
alter table Taco_Ingredients add foreign key (ingredients_id) references Ingredient(id);

create table if not exists Taco_Order (
	id identity,
	name varchar(50) not null,
	street varchar(50) not null,
	city varchar(50) not null,
	state varchar(20),
	zip varchar(10) not null,
	cc_Number varchar(16) not null,
	cc_Expiration varchar(5) not null,
	ccCVV varchar(3) not null,
	placed_At timestamp not null,
	user_id bigint
);
alter table Taco_Order add foreign key (user_id) references Taco_users(id);

create table if not exists Taco_Order_Tacos (
	order_id bigint not null,
	tacos_id bigint not null
);

alter table Taco_Order_Tacos add foreign key (order_id) references Taco_Order(id);
alter table Taco_Order_Tacos add foreign key (tacos_id) references Taco(id);

delete from Taco_Order_Tacos;
delete from Taco_Ingredients;
delete from Taco;
delete from Taco_Order;
delete from Ingredient;
insert into Ingredient (id, name, type) values ('FLTO', 'Flour Tortilla', 'WRAP');
insert into Ingredient (id, name, type) values ('COTO', 'Corn Tortilla', 'WRAP');
insert into Ingredient (id, name, type) values ('GRBF', 'Ground Beef', 'PROTEIN');
insert into Ingredient (id, name, type) values ('CARN', 'Carnitas', 'PROTEIN');
insert into Ingredient (id, name, type) values ('TMTO', 'Diced Tomatoes', 'VEGGIES');
insert into Ingredient (id, name, type) values ('LETC', 'Lettuce', 'VEGGIES');
insert into Ingredient (id, name, type) values ('CHED', 'Cheddar', 'CHEESE');
insert into Ingredient (id, name, type) values ('JACK', 'Monterrey Jack', 'CHEESE');
insert into Ingredient (id, name, type) values ('SLSA', 'Salsa', 'SAUCE');
insert into Ingredient (id, name, type) values ('SRCR', 'Sour Cream', 'SAUCE');


-----

insert into Authority (id, authority) values (1, 'ROLE_TEST');
insert into Authority (id, authority) values (2, 'ROLE_USER');

insert into Taco_users (id, username, password, enabled, fullname, street, city, zip, phone_number)
values (1, 'test', '$2a$10$kltFIPQ92W9/rROqyN21ROL8jD2bPRvA1QO8wl2dJ8krXM3sOuRGi', 1, 'Mister Test', '1 Marveaux avenue', 'Paris', '75000', '12345'); 
insert into Taco_users (id, username, password, enabled, fullname, street, city, zip, phone_number)
values (2, 'user', '$2a$10$7srN2o9kg2B0ujBIwCV8duAIyTUZTnLlUYiIMCTRY1E2omvxejNFC', 1, 'Boss User', '24 boulevard des stars', 'Bordeaux', '35000', '54321');

insert into Taco_users_authorities (taco_users_id, authorities_id) values (1, 1);
insert into Taco_users_authorities (taco_users_id, authorities_id) values (2, 2);
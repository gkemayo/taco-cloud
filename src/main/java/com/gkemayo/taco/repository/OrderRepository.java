package com.gkemayo.taco.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.taco.entity.Order;
import com.gkemayo.taco.entity.User;

@Repository("orderRepo")
public interface OrderRepository extends CrudRepository<Order, Long> {
	
	List<Order> findByZip(String zip);
	
	List<Order> findByUserOrderByPlacedAtDesc(User user, Pageable pageable);
	
	
}

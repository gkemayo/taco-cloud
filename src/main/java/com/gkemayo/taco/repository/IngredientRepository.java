package com.gkemayo.taco.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.taco.entity.Ingredient;

@Repository("ingredientRepo")
public interface IngredientRepository extends CrudRepository<Ingredient, String> {
	
}

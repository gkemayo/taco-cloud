package com.gkemayo.taco.repository;



import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.taco.entity.Taco;

@Repository("designRepo")
public interface TacoRepository extends CrudRepository<Taco, Long> {
	
}

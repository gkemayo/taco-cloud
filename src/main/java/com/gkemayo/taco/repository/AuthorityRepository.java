package com.gkemayo.taco.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gkemayo.taco.entity.Authority;

@Repository
public interface AuthorityRepository extends CrudRepository<Authority, Long> {
	
}

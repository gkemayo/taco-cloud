package com.gkemayo.taco.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gkemayo.taco.entity.RegistrationForm;
import com.gkemayo.taco.repository.UserRepository;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/register")
public class RegistrationController {
	
	@Autowired
	private UserRepository userRepo;
	
	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
//	public RegistrationController(UserRepository userRepo, BCryptPasswordEncoder passwordEncoder) {
//		this.userRepo = userRepo;
//		this.passwordEncoder = passwordEncoder;
//	}
	
	@GetMapping
	public String registerForm() {
		log.info("Calling GET /register");
		return "registration";
	}
	
	@PostMapping
	public String processRegistration(RegistrationForm form) {
		log.info("Calling POST /register");
		userRepo.save(form.toUser(passwordEncoder));
		return "redirect:/login";
	}
}

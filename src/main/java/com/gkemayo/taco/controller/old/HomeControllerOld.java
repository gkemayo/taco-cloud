package com.gkemayo.taco.controller.old;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeControllerOld {
	
	@GetMapping("/old")
	public String home() {
		return "home";
	}   

}

/**
 * 
 */
package com.gkemayo.taco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
//	@Autowired
//	private DataSource dataSource;
	
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//			.withUser("test")
//			.password("{noop}test1")
//			.authorities("ROLE_TEST")
//		.and()
//			.withUser("user")
//			.password("{noop}user1")
//			.authorities("ROLE_USER");
//				
//		//		Use {bcrypt} for BCryptPasswordEncoder,
//		//		Use {noop} for NoOpPasswordEncoder,
//		//		Use {pbkdf2} for Pbkdf2PasswordEncoder, 
//		//		Use {scrypt} for SCryptPasswordEncoder,
//		//		Use {sha256} for StandardPasswordEncoder.
//		
//	}
	
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.jdbcAuthentication()
//			.dataSource(dataSource)
//			.usersByUsernameQuery("select username, password, enabled from Taco_users where username = ?")
//			.authoritiesByUsernameQuery("select username, authority from Taco_users_authorities where username = ?")
//			.passwordEncoder(new BCryptPasswordEncoder());
//		
//	}
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(customUserDetailsService)
			.passwordEncoder(new BCryptPasswordEncoder());
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
			.antMatchers("/design", "/orders", "/orders/current").access("isAuthenticated() and hasAnyRole('ROLE_USER', 'ROLE_TEST')")
			.antMatchers("/", "/register", "/h2-console/**", "/**").access("permitAll")
		.and()
			.formLogin()
				.loginPage("/login")
				.defaultSuccessUrl("/design")
		.and()
			.logout()
				.logoutSuccessUrl("/login");
		
		//désactive les options ci-dessous pour avoir accès à la console H2
		http.csrf().disable();
	    http.headers().frameOptions().disable();
	}
	
}
